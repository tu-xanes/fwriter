#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## test
#epicsEnvSet(PREFIX, "XNS:SUP:")

# Macros
epicsEnvSet(PREFIX, "$(IOCBL):$(IOCDEV):")
epicsEnvSet(P, $(PREFIX))

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

dbLoadTemplate("fwriter.subs", "P=$(P)")

# autosave
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

# autosave
create_monitor_set("auto_settings.req", 30, "P=$(P)")

## Start any sequence programs
#seq sncxxx,"user=epics"
